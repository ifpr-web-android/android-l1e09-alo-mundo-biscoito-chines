package com.example.everaldo.l1e9biscoitochines;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {


    private String[] frases;
    private Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frases = getResources().getStringArray(R.array.sorte_do_dia);
        random = new Random();
        gerarSorte();


    }


    public void tenteDeNovo(View view){
        gerarSorte();
    }


    private void gerarSorte(){
        int numero_sorte = random.nextInt(frases.length);
        TextView sorte_do_dia = (TextView) findViewById(R.id.sorte_do_dia);
        sorte_do_dia.setText(frases[numero_sorte]);
    }

}
